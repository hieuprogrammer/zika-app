package io.hieu.main;

import io.hieu.chart.JFreeChartDrawer;
import io.hieu.csv.CSVParser;
import io.hieu.csv.data.H2DbConnect;
import io.hieu.models.State;

import java.util.*;

public class Main {
    private static final CSVParser csvParser = new CSVParser();
    private static final H2DbConnect h2DbConnect = new H2DbConnect();
    private static JFreeChartDrawer jFreeChartDrawer = new JFreeChartDrawer();

    public static void main(String[] args) {
        System.out.println("Zika Statistical Chart App");

        h2DbConnect.h2DbConnect();
        h2DbConnect.insertData(csvParser.readCSVFilesInDirectory());
//        h2DbConnect.queryData();
        List<String> states = h2DbConnect.queryStateData();
        for (String state : states) {
            System.out.println(state); // 55 states in total.
        }
        System.out.println("Total states reported: " + states.size());
        h2DbConnect.queryStateData();

        Set<State> data = csvParser.readCSVFilesInDirectory();
        for (State state : data) {
            System.out.println(state.getData());
        }

        List<Integer> affectedDatesData = h2DbConnect.queryAffectedCasesData();
        for (int affectedDate : affectedDatesData) {
            System.out.println(affectedDate);
        }
        List<Date> datesData = h2DbConnect.queryDatesData();
        for (Date date : datesData) {
            System.out.println(date);
        }

        for (State state : data) {
            Map<String, List<Integer>> stateData = state.getData();
            jFreeChartDrawer = new JFreeChartDrawer(state.getName(), stateData);
            jFreeChartDrawer.drawChart();
        }
    }
}