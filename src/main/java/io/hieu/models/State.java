package io.hieu.models;

import java.util.List;
import java.util.TreeMap;

public class State {
    private String name;
    private TreeMap<String, List<Integer>> data;

    public State() {
    }

    public State(String name, TreeMap<String, List<Integer>> data) {
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TreeMap<String, List<Integer>> getData() {
        return data;
    }

    public void setData(TreeMap<String, List<Integer>> data) {
        this.data = data;
    }
}