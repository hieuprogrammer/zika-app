package io.hieu.csv.data;

import io.hieu.models.State;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class H2DbConnect {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:~/test";
    private static final String USER = "sa";
    private static final String PASS = "";

    public void h2DbConnect() {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql = "DROP TABLE IF EXISTS ZIKA_DATA; " +
                    "CREATE TABLE ZIKA_DATA " +
                    "(id INTEGER NOT NULL AUTO_INCREMENT," +
                    "STATE VARCHAR(255)," +
                    "DATE VARCHAR(255)," +
                    "AFFECTED_CASES INTEGER," +
                    "TOTAL INTEGER," +
                    " PRIMARY KEY (id))";
            stmt.executeUpdate(sql);

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void insertData(Set<State> stateData) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to H2 database.");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Successfully connected to the database.");

            for (State state : stateData) {
                stmt = conn.createStatement();
                String sql = String.format("INSERT INTO ZIKA_DATA(STATE, DATE, AFFECTED_CASES, TOTAL) VALUES('%s', PARSEDATETIME('%s', 'yyyy-MM-dd'), %d, %d)",
                        state.getName(),
                        state.getData().firstEntry().getKey(),
                        state.getData().firstEntry().getValue().get(0),
                        state.getData().firstEntry().getValue().get(1));
                stmt.executeUpdate(sql);
            }

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void queryData() {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql = String.format("SELECT STATE, DATE, AFFECTED_CASES, TOTAL FROM ZIKA_DATA");
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String state = rs.getString(1);
                String date = rs.getString(2);
                int affectedCases = rs.getInt(3);
                int total = rs.getInt(4);

                System.out.println("STATE: " + state + " | DATE: " + date + " | AFFECTED_CASES: " + affectedCases + " | TOTAL: " + total);
            }

            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
                try {
                    if (conn != null) conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        }
    }

    public List<String> queryStateData() {
        List<String> states = new ArrayList<String>();
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql = String.format("SELECT DISTINCT STATE FROM ZIKA_DATA");
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String state = rs.getString(1);
                states.add(state);
//                System.out.println("STATE: " + state);
            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
                try {
                    if (conn != null) conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        }

        return states;
    }

    public List<Date> queryDatesData() {
        List<Date> dates = new ArrayList<Date>();
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql = String.format("SELECT DATE FROM ZIKA_DATA");
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(1));
                dates.add(date);
//                System.out.println("AFFECTED_CASE: " + affectedCase);
            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
                try {
                    if (conn != null) conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        }

        return dates;
    }

    public List<Integer> queryAffectedCasesData() {
        List<Integer> affectedCases = new ArrayList<Integer>();
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql = String.format("SELECT AFFECTED_CASES FROM ZIKA_DATA");
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int affectedCase = rs.getInt(1);
                affectedCases.add(affectedCase);
//                System.out.println("AFFECTED_CASE: " + affectedCase);
            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
                try {
                    if (conn != null) conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        }

        return affectedCases;
    }
}