package io.hieu.csv;

import com.opencsv.CSVReader;
import io.hieu.models.State;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CSVParser {
    public Set<State> readCSVFilesInDirectory() {
        File[] filesInDirectory = new File("src/main/resources/input").listFiles();
        Set<State> states = new HashSet<State>();
        for (File file : filesInDirectory) {
            CSVReader reader = null;
            try {
                reader = new CSVReader(new FileReader(file.getAbsolutePath()));
                String[] line = reader.readNext();
                while ((line = reader.readNext()) != null) {
                    TreeMap<String, List<Integer>> data = new TreeMap<String, List<Integer>>();
                    String dataWithoutSpecialCharacters = line[0].replace("‡", "").replace("†", "");
                    String dateFromFileName = file.getName();
                    List<Integer> value = new ArrayList<>();
                    value.add(Integer.parseInt(line[1]));
                    value.add(Integer.parseInt(line[2]));
                    data.put(dateFromFileName.substring(dateFromFileName.indexOf("2"), dateFromFileName.indexOf(".")), value);
                    states.add(new State(dataWithoutSpecialCharacters, data));
//                    System.out.println("Data[State: " + line[0] + " - Mixed Data Column 1: " + line[1] + " - Mixed Data Column 2: " + line[2] + "]");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Parsing dates data:
        List<String> dates = new ArrayList<String>();
        for (State state : states) {
            dates.add(state.getData().firstEntry().getKey());
        }
        Collections.sort(dates);
//
//        for (String date : dates) {
//            System.out.println(date);
//        }

//        for (State state : states) {
//            System.out.println(state.getName() + " | " + state.getData().firstEntry().getKey() + " | " + state.getData().firstEntry().getValue());
//        }

//        System.out.println(states.size());
        return states;
    }
}